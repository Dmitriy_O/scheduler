package com.playtika.scheduler.tests;

import org.junit.Assert;
import org.junit.Test;

import com.playtika.automation.http.client.teamcity.pojo.TeamCityRun;
import com.playtika.automation.utils.XmlBuilder;

public class XmlBuilderTest extends BaseTest {

    @Test
    public void shouldReplaceData() {
        XmlBuilder xmlBuilder = new XmlBuilder();
        String expectedData = xmlBuilder.getXmlData()
                                        .replace("$parentJobId", "myParentJobId")
                                        .replace("$environment", "stage:cc_stage_95")
                                        .replace("$testsBuildBranch", "master")
                                        .replace("$build", "myBuildUrl")
                                        .replace("$browser", "chrome")
                                        .replace("$clientPlatform", "facebook")
                                        .replace("$appiumHubUrl", "appiumHub")
                                        .replace("$selenoidUrl", "webDriverHub")
                                        .replace("$selectiveTags", "case-id:70000&amp;case-id:1345678")
                                        .replace("$gaugeTags", "gaugeTags")
                                        .replace("$mutedGaugeTags", "mutedGaugeTags")
                                        .replace("$eurekaTags", "(service:infra-eureka-server|service:EUREKA)&amp;!muted")
                                        .replace("$trsEnabled", "true")
                                        .replace("$runId", "testRailRunId");
        TeamCityRun actualRun = TeamCityRun.builder()
                                           .parentJobId("myParentJobId")
                                           .environment("stage:cc_stage_95")
                                           .testBuildBranch("master")
                                           .platform("win32")
                                           .appiumHubUrl("appiumHub")
                                           .webDriverHubUrl("webDriverHub")
                                           .browser("chrome")
                                           .clientPlatform("facebook")
                                           .buildUrl("myBuildUrl")
                                           .selectiveGaugeTags("case-id:70000&amp;case-id:1345678")
                                           .gaugeTags("gaugeTags")
                                           .mutedGaugeTags("mutedGaugeTags")
                                           .eurekaTags("(service:infra-eureka-server|service:EUREKA)&amp;!muted")
                                           .testRailRunId("testRailRunId")
                                           .trsStubEnabled(true)
                                           .build();
        String actualData = xmlBuilder.replaceData(actualRun);
        Assert.assertEquals(expectedData, actualData);
    }
}
