package com.playtika.scheduler.tests;

import org.junit.Assert;
import org.junit.Test;

import com.playtika.automation.utils.TagParser;


public class TagsParserTest {

    @Test
    public void shouldParseTags() {
        String expectedTags  = "case-id:70000&amp;case-id:1345678|dotcom-case-id:777777";
        String tagsToParse = "case-id:70000 & case-id:1345678 |dotcom-case-id:777777";
        String actualTags = TagParser.parseSelectiveTags(tagsToParse);
        Assert.assertEquals("Tags were not parsed correctly", expectedTags, actualTags);
    }
}
