package com.playtika.scheduler.tests;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.junit.Assert;
import org.junit.Test;

import com.playtika.automation.http.client.artifactory.ArtifactoryClient;
import com.playtika.automation.utils.UrlBuilder;

public class UrlBuilderTest extends BaseTest {
    private static OkHttpClient client = new OkHttpClient();

    private boolean isValid(String url) {
        try {
            new URL(url).toURI();
            return true;
        } catch (MalformedURLException | URISyntaxException e) {
            return false;
        }
    }

    private int getUrl(String url) {
        int code = 0;
        Request request = new Request.Builder()
                .url(url)
                .get()
                .header("Authorization", Credentials.basic("dotrashevskiy", "Qzxc589_+"))
                .addHeader("Content-Type", "text/plain; charset=utf-8")
                .build();
        try {
            code = client.newCall(request).execute().code();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return code;
    }

    @Test
    public void shouldBuildValidUrls() {
        int expectedStatusCode = 200;
        UrlBuilder urlBuilder = new UrlBuilder(new ArtifactoryClient());
        Map<String, String> buildUrls = urlBuilder.resolveMobileBuildVersions();
        Assert.assertNotNull("Url were not retrieved!", buildUrls);
        buildUrls.forEach((platform, url) -> {
            Assert.assertTrue("URL is not valid!", isValid(url));
            Assert.assertEquals("Status code during GET is not success!", expectedStatusCode, getUrl(url));
        });
    }
}
