package com.playtika.scheduler.tests;

import org.junit.Test;

import com.playtika.automation.http.client.artifactory.ArtifactoryClient;
import com.playtika.automation.http.client.teamcity.pojo.TeamCityRun;
import com.playtika.automation.utils.RunBuilder;
import com.playtika.automation.utils.UrlBuilder;

public class RunBuilderTest extends BaseTest {

    @Test
    public void shouldBuildRun() {
        TeamCityRun expectedRun = TeamCityRun.builder()
                                           .parentJobId("myParentJobId")
                                           .platform("win32")
                                           .appiumHubUrl("appiumHub")
                                           .webDriverHubUrl("webDriverHub")
                                           .browser("chrome")
                                           .clientPlatform("facebook")
                                           .buildUrl("myBuildUrl")
                                           .gaugeTags("gaugeTags")
                                           .mutedGaugeTags("mutedGaugeTags")
                                           .eurekaTags("eurekaTags")
                                           .testRailRunId("testRailRunId")
                                           .build();
        RunBuilder runBuilder = new RunBuilder(properties, new UrlBuilder(new ArtifactoryClient()));
        runBuilder.prepareRuns();
    }
}
