package com.playtika.automation.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.google.common.collect.ImmutableMap;

import com.playtika.automation.http.client.teamcity.pojo.TeamCityRun;

import static com.playtika.automation.utils.TagParser.parseSelectiveTags;

public class RunBuilder {
    private Properties properties;
    private Map<String, String> mobileBuildUrls;
    private Map<String, String> localUrls;
    private Map<String, String> testRailRuns;

    public RunBuilder(Properties properties, UrlBuilder urlBuilder) {
        this.properties = properties;
        this.mobileBuildUrls = urlBuilder.resolveMobileBuildVersions();
        localUrls = ImmutableMap.of("android" , properties.getProperty("android_hub_url"),
                                    "android-pras", properties.getProperty("android_hub_url"),
                                    "ios", properties.getProperty("ios_hub_url"));
        testRailRuns = ImmutableMap.of("android", properties.getProperty("android_run_id"),
                                       "android-pras", properties.getProperty("pras_run_id"),
                                       "ios", properties.getProperty("ios_run_id"),
                                       "win32", properties.getProperty("win_32_run_id"),
                                       "windows10", properties.getProperty("win_10_run_id"));
    }

    private String resolveParentJobId(String platform) {
        String parentJobId;
        if (platform.equals("api")) {
            parentJobId = properties.getProperty("api_job_id");
        } else {
            parentJobId = properties.getProperty("ui_job_id");
        }
        return parentJobId;
    }

    private String resolveHubUrl(String platform) {
        String hubUrl;
        if (localUrls.keySet().contains(platform)) {
            hubUrl = localUrls.get(platform);
        } else {
            hubUrl = properties.getProperty("selenoid_url");
        }
        return hubUrl;
    }

    private List<String> resolveBrowserAndPlatform(String platform) {
        List<String> browserAndPlatform = Arrays.asList(platform.split(" "));
        if (browserAndPlatform.size() == 1) {
            browserAndPlatform.set(0, " ");
        }
        return browserAndPlatform;
    }

    private List<String> splitGaugeTags(String platform) {
        String platformTagKey = platform.trim().replace(" ", "_") + "_tags";
        return Arrays.asList(properties.getProperty(platformTagKey).split("\\^"));
    }

    private String resolveTestRailRunId(String platform) {
        String runId = "";
        if (testRailRuns.keySet().contains(platform)) {
            runId = testRailRuns.get(platform);
        }
        return runId;
    }

    private String resolveMobileBuildUrl(String platform) {
        String buildUrl = "";
        if (mobileBuildUrls.keySet().contains(platform.trim())) {
            buildUrl = mobileBuildUrls.get(platform.trim());
        }
        return buildUrl;
    }

    private String resolveEnvironment() {
        String environment = properties.getProperty("environment");
        if (environment.equals("preprod")) {
            return environment;
        } else {
            return "stage:" + environment;
        }
    }

    private String resolveEurekaTags() {
        String environment = properties.getProperty("environment");
        String eurekaTags = properties.getProperty("eureka_tags");
        if (environment.equals("preprod")) {
            return eurekaTags + "&amp;!StageOnly";
        } else {
            return eurekaTags;
        }
    }

    private boolean resolveTrsStub() {
        String environment = properties.getProperty("environment");
        if (environment.equals("preprod")) {
            return false;
        } else {
            return true;
        }
    }

    public List<TeamCityRun> prepareRuns() {
        List<String> platforms = Arrays.asList(properties.getProperty("platforms").split(";"));
        List<TeamCityRun> runs = new ArrayList<>();
        for (String platform : platforms) {
            List<String> gaugeTagList = splitGaugeTags(platform);
            String hubUrl = resolveHubUrl(platform);
            List<String> browserAndPlatform = resolveBrowserAndPlatform(platform);
            String buildUrl = resolveMobileBuildUrl(platform);
            TeamCityRun run = TeamCityRun.builder()
                                         .platform(platform)
                                         .parentJobId(resolveParentJobId(platform))
                                         .appiumHubUrl(hubUrl)
                                         .webDriverHubUrl(hubUrl)
                                         .browser(browserAndPlatform.get(0))
                                         .clientPlatform(browserAndPlatform.get(1))
                                         .gaugeTags(gaugeTagList.get(0))
                                         .mutedGaugeTags(gaugeTagList.get(1))
                                         .testRailRunId(resolveTestRailRunId(platform))
                                         .buildUrl(buildUrl)
                                         .environment(resolveEnvironment())
                                         .eurekaTags(resolveEurekaTags())
                                         .trsStubEnabled(resolveTrsStub())
                                         .testBuildBranch(properties.getProperty("tests_build_branch"))
                                         .selectiveGaugeTags(parseSelectiveTags(properties.getProperty("selective_gauge_tags")))
                                         .build();
            runs.add(run);
        }
        return runs;
    }
}
