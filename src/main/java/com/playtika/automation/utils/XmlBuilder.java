package com.playtika.automation.utils;

import lombok.Getter;

import com.playtika.automation.http.client.teamcity.pojo.TeamCityRun;

@Getter
public class XmlBuilder {
    private String xmlData = "<build><buildType id='$parentJobId'/><properties>" +
            "<property name='reverse.dep.*.testEnvironment' value='$environment'/>" +
            "<property name='reverse.dep.*.teamcity.build.branch' value='$testsBuildBranch'/>" +
            "<property name='reverse.dep.*.selective.gauge.tags' value='$selectiveTags'/>" +
            "<property name='reverse.dep.*.env.appiumDriver.appUri' value='$build'/>" +
            "<property name='reverse.dep.*.env.browser' value='$browser'/>" +
            "<property name='reverse.dep.*.env.client.platform' value='$clientPlatform'/>" +
            "<property name='reverse.dep.*.env.appiumDriver.hubUrl' value='$appiumHubUrl'/>" +
            "<property name='reverse.dep.*.env.webDriver.hubUrl' value='$selenoidUrl'/>" +
            "<property name='reverse.dep.*.eureka.tags' value='$eurekaTags'/>" +
            "<property name='reverse.dep.*.env.trs.stub.enabled' value='$trsEnabled'/>" +
            "<property name='reverse.dep.caesars_ui_tests_Tests_ManualRunsChild_MultiPlatformUi.gauge.tags' value='$gaugeTags'/>" +
            "<property name='reverse.dep.caesars_ui_tests_Tests_ManualRunsChild_MultiPlatformUiMuted.gauge.tags' value='$mutedGaugeTags'/>" +
            "</properties>" +
            "</build>";

    public String replaceData(TeamCityRun run) {
        return xmlData
                .replace("$parentJobId", run.getParentJobId())
                .replace("$environment", run.getEnvironment())
                .replace("$testsBuildBranch", run.getTestBuildBranch())
                .replace("$build", run.getBuildUrl())
                .replace("$browser", run.getBrowser())
                .replace("$clientPlatform", run.getClientPlatform())
                .replace("$appiumHubUrl", run.getAppiumHubUrl())
                .replace("$selenoidUrl", run.getWebDriverHubUrl())
                .replace("$selectiveTags", run.getSelectiveGaugeTags())
                .replace("$gaugeTags", run.getGaugeTags())
                .replace("$mutedGaugeTags", run.getMutedGaugeTags())
                .replace("$eurekaTags", run.getEurekaTags())
                .replace("$trsEnabled", String.valueOf(run.isTrsStubEnabled()))
                .replace("$runId", run.getTestRailRunId());
    }
}
