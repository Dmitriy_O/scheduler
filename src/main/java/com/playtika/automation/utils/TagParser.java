package com.playtika.automation.utils;

public class TagParser {

    private static String removeTagSpaces(String tagsToParse) {
        String[] tags = tagsToParse.split(" ");
        String newTags = "";
        for (String tag : tags) {
            newTags += tag.trim();
        }
        return newTags;
    }

    public static String parseSelectiveTags(String tagsToParse) {
        String tags = "";
        String selective_gauge_tags = removeTagSpaces(tagsToParse);
        String[] splitTags = selective_gauge_tags.split("&");
        int counter = 0;
        while (counter < splitTags.length - 1) {
            tags += splitTags[counter].trim() + "&amp;";
            counter++;
        }
        tags += splitTags[counter].trim();
        return tags;
    }
}
