package com.playtika.automation.utils;

import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {
    private static Properties properties = new Properties();

    public static Properties getProperties() {
        try {
            properties.load(PropertyLoader.class.getClassLoader().getResourceAsStream("config.properties"));
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return properties;
    }
}
