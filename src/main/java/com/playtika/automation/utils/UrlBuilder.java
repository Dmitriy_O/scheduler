package com.playtika.automation.utils;

import java.util.HashMap;
import java.util.Map;

import com.playtika.automation.http.client.artifactory.ArtifactoryClient;

public class UrlBuilder {
    private ArtifactoryClient artifactoryClient;

    public UrlBuilder(ArtifactoryClient artifactoryClient) {
        this.artifactoryClient = artifactoryClient;
    }

    public Map<String, String> resolveMobileBuildVersions() {
        Map<String, String> mobileBuildUrlMap = new HashMap<>();
        mobileBuildUrlMap.put("win32", artifactoryClient.getLatestBuildByVersion("com.playtika.caesarscasino.Win32"));
        mobileBuildUrlMap.put("windows10", artifactoryClient.getLatestBuildByVersion("com.playtika.caesarscasino.Win10"));
        mobileBuildUrlMap.put("android", artifactoryClient.getLatestBuildByVersion("com.playtika.caesarscasino.ARM.dev.1136x640Signed"));
        mobileBuildUrlMap.put("android-pras", artifactoryClient.getLatestBuildByVersion("com.playtika.caesarscasino-pras.ARM.dev.1136x640Signed"));
        mobileBuildUrlMap.put("ios", artifactoryClient.getLatestBuildByVersion("CaesarsSlotsiPhone"));
        return mobileBuildUrlMap;
    }
}
