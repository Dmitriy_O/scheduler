package com.playtika.automation.http.client.artifactory;

import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.jfrog.artifactory.client.Artifactory;
import org.jfrog.artifactory.client.ArtifactoryClientBuilder;
import org.jfrog.artifactory.client.model.File;
import org.jfrog.artifactory.client.model.RepoPath;

import com.playtika.automation.utils.PropertyLoader;

public class ArtifactoryClient {
    private static Properties properties = PropertyLoader.getProperties();
    private String repository = properties.getProperty("artifactory_repository");
    private String buildVersion = properties.getProperty("mobile_build_version");
    private Artifactory artifactory = ArtifactoryClientBuilder.create()
                                                              .setUrl(properties.getProperty("artifactory_base_uri"))
                                                              .setUsername(properties.getProperty("jfrog_user"))
                                                              .setPassword(properties.getProperty("jfrog_pswd"))
                                                              .build();
    private static final String END_OF_MAJOR_VERSION_PATTERN = "-";

    public String getLatestBuildByVersion(String platform) {
        List<RepoPath> buildPaths = doSearch(platform);
        List<File> files = getFiles(buildPaths, buildVersion);
        List<String> urls = mapUrlsFromFiles(files);
        return urls.get(0);
    }

    private List<File> getFiles(List<RepoPath> repoPaths, String version) {
        List<RepoPath> filteredRepoPaths = filterRepoPaths(repoPaths, version);
        return filteredRepoPaths.stream()
                                .map(i -> (File) artifactory.repository(repository).file(i.getItemPath()).info())
                                .collect(Collectors.toList());
    }

    private List<RepoPath> doSearch(String platform) {
        return artifactory.searches().repositories(repository).artifactsByName(platform).doSearch();
    }

    private List<String> mapUrlsFromFiles(List<File> files) {
        return files.stream()
                    .sorted(Comparator.comparing(File::getCreated).reversed())
                    .map(File::getDownloadUri)
                    .collect(Collectors.toList());
    }

    private List<RepoPath> filterRepoPaths(List<RepoPath> repoPaths, String version) {
        return repoPaths.stream()
                        .filter(p -> p.getItemPath().contains(version + END_OF_MAJOR_VERSION_PATTERN))
                        .collect(Collectors.toList());
    }
}
