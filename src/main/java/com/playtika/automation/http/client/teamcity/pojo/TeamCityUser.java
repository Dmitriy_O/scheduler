package com.playtika.automation.http.client.teamcity.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class TeamCityUser {

    @JsonProperty("username")
    private String userName;
}
