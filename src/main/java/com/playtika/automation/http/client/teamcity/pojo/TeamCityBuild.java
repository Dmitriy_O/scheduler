package com.playtika.automation.http.client.teamcity.pojo;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@ToString
public class TeamCityBuild {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("buildTypeId")
    private String buildTypeId;

    @JsonProperty("state")
    private String state;

    @JsonProperty("href")
    private String href;

    @JsonProperty("webUrl")
    private String webUrl;

    @JsonProperty("triggered")
    private Triggered triggered;

    @JsonProperty("properties")
    private TeamCityProperties properties;

    public String resolvePlatform() {
        List<TeamCityProperty> properties = this.getProperties()
                                                 .getProperty()
                                                 .stream()
                                                 .filter(p -> p.getName().equals("reverse.dep.*.env.browser")
                                                         || p.getName().equals("reverse.dep.*.env.client.platform"))
                                                 .collect(Collectors.toList());
        return (properties.get(0).getValue() + " " +  properties.get(1).getValue()).trim();
    }
}
