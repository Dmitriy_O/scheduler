package com.playtika.automation.http.client;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

public class BaseHttpClient {

    protected OkHttpClient getClient() {
        return new OkHttpClient();
    }

    protected Response checkResponse(Response response) throws IOException {
        Response responseToReturn = null;
        if (response.code() != 200) {
            System.out.println("There was a bad response with %s code and %s content".format(String.valueOf(response.code()),
                                                                                             response.body().string()));
            System.exit(1);
        } else {
            responseToReturn = response;
        }
        return responseToReturn;
    }
}
