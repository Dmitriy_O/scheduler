package com.playtika.automation.http.client.teamcity.pojo;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class TeamCityRun {
    private String platform;
    private String parentJobId;
    private String appiumHubUrl;
    private String webDriverHubUrl;
    private String browser;
    private String clientPlatform;
    private String buildUrl;
    private String gaugeTags;
    private String mutedGaugeTags;
    private String eurekaTags;
    private String testRailRunId;
    private String environment;
    private boolean trsStubEnabled;
    private String selectiveGaugeTags;
    private String testBuildBranch;
}
