package com.playtika.automation.http.client.teamcity;

import java.io.IOException;
import java.util.Properties;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import com.playtika.automation.http.client.BaseHttpClient;
import com.playtika.automation.http.client.teamcity.pojo.TeamCityBuild;
import com.playtika.automation.http.client.teamcity.pojo.TeamCityRun;
import com.playtika.automation.utils.PropertyLoader;
import com.playtika.automation.utils.XmlBuilder;

@Slf4j
public class TeamCityClient extends BaseHttpClient {
    private static Properties properties = PropertyLoader.getProperties();
    private static final String USER_NAME = properties.getProperty("teamcity_user_name");
    private static final String PASSWORD = properties.getProperty("teamcity_password");
    private static final String BASE_URI = properties.getProperty("teamcity_base_url");
    private ObjectMapper mapper = new ObjectMapper();
    private XmlBuilder xmlBuilder = new XmlBuilder();

    public TeamCityBuild scheduleTeamCityJob(TeamCityRun run) throws IOException {
        String xmlData = xmlBuilder.replaceData(run);
        MediaType mediaType = MediaType.get("application/xml; charset=utf-8");

        RequestBody body = RequestBody.create(mediaType, xmlData);
        Request request = new Request.Builder()
                .url(BASE_URI + "buildQueue")
                .post(body)
                .header("Authorization", Credentials.basic(USER_NAME, PASSWORD))
                .addHeader("Accept", "application/json; charset=utf-8")
                .build();
        Response response = getClient().newCall(request).execute();
        checkResponse(response);
        TeamCityBuild build = mapper.readValue(response.body().string(), TeamCityBuild.class);
        response.body().close();
        log.info(String.format("Scheduled for %s platform with %s url", build.resolvePlatform(), build.getWebUrl()));
        return build;
    }

    public String getBuildState(Long buildId) throws IOException {

        Request request = new Request.Builder()
                .url(BASE_URI + "builds/id:" + buildId + "/state")
                .get()
                .header("Authorization", Credentials.basic(USER_NAME, PASSWORD))
                .addHeader("Content-Type", "text/plain; charset=utf-8")
                .build();
        Response response = getClient().newCall(request).execute();
        checkResponse(response);
        String responseBody = response.body().string();
        response.body().close();
        return responseBody;
    }

    public String getBuildStatus(Long buildId) throws IOException {
        Request request = new Request.Builder()
                .url(BASE_URI + "builds/aggregated/id:" + buildId + "/status")
                .get()
                .header("Authorization", Credentials.basic(USER_NAME, PASSWORD))
                .addHeader("Content-Type", "text/plain; charset=utf-8")
                .build();
        Response response = getClient().newCall(request).execute();
        checkResponse(response);
        String responseBody = response.body().string();
        response.body().close();
        return responseBody;
    }

    public void attachTags(String buildId, String platform, String userName, String environment) throws IOException {
        String tags = "<tags><tag name='$tags'/></tags>"
                .replace("$tags", String.format("%s_%s_%s", userName, environment, platform));
        MediaType mediaType = MediaType.get("application/xml; charset=utf-8");
        RequestBody body = RequestBody.create(mediaType, tags);
        Request request = new Request.Builder()
                .url(BASE_URI + "builds/id:" + buildId + "/tags")
                .post(body)
                .header("Authorization", Credentials.basic(USER_NAME, PASSWORD))
                .addHeader("Content-Type", "application/xml; charset=utf-8")
                .build();
        Response response = getClient().newCall(request).execute();
        checkResponse(response);
        log.info(String.format("Attached tags for the build with %s id", buildId));
        response.body().close();
    }
}
