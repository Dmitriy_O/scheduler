package com.playtika.automation.http.client.teamcity.pojo;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@ToString
public class TeamCityProperties {

    @JsonProperty("count")
    private long count;

    @JsonProperty("property")
    private List<TeamCityProperty> property;
}
