import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import lombok.extern.slf4j.Slf4j;

import com.playtika.automation.http.client.artifactory.ArtifactoryClient;
import com.playtika.automation.http.client.teamcity.TeamCityClient;
import com.playtika.automation.http.client.teamcity.pojo.TeamCityBuild;
import com.playtika.automation.http.client.teamcity.pojo.TeamCityRun;
import com.playtika.automation.utils.PropertyLoader;
import com.playtika.automation.utils.RunBuilder;
import com.playtika.automation.utils.UrlBuilder;


@Slf4j
public class Scheduler {
    private static int buildCounter = 0;
    private static int numberOfRuns = 2;
    private static Properties properties = PropertyLoader.getProperties();
    private static TeamCityClient teamCityClient = new TeamCityClient();
    private static UrlBuilder urlBuilder = new UrlBuilder(new ArtifactoryClient());
    private static RunBuilder runBuilder = new RunBuilder(properties, urlBuilder);
    private static List<String> statuses;

    public static void main(String[] args) throws IOException, InterruptedException {
        checkPlatformsAreSelected();
        List<TeamCityRun> teamCityRuns = runBuilder.prepareRuns();
        do {
            buildCounter++;
            log.info(String.format("***********************Starting jobs for run #%d***********************", buildCounter));
            statuses = checkJobIsFinished(scheduleBuilds(teamCityRuns));
        } while (buildCounter < numberOfRuns);
        verifyMainBuildStatus(statuses);
    }

    public static void checkPlatformsAreSelected() {
        if (properties.getProperty("platforms").split(" ").length == 0) {
            log.info("Please check at least one platform for the run! Terminating...");
            System.exit(1);
        }
    }

    public static List<String> checkJobIsFinished(List<TeamCityBuild> builds) throws IOException, InterruptedException {
        List <String> statuses = new ArrayList<>();
        for (TeamCityBuild build: builds) {
            String buildState = teamCityClient.getBuildState(build.getId());
            while (!buildState.equals("finished")) {
                Thread.sleep(10000);
                buildState = teamCityClient.getBuildState(build.getId());
            }
            String buildStatus = teamCityClient.getBuildStatus(build.getId());
            log.info(String.format("Job for platform %s with %s link has status: %s", build.resolvePlatform(), build.getWebUrl(), buildStatus));
            statuses.add(buildStatus);
        }
        return statuses;
    }

    public static List<TeamCityBuild> scheduleBuilds(List<TeamCityRun> teamCityRuns) throws IOException {
        List<TeamCityBuild> builds = new ArrayList<>();
        for (TeamCityRun run : teamCityRuns) {
            TeamCityBuild build = teamCityClient.scheduleTeamCityJob(run);
            teamCityClient.attachTags(String.valueOf(build.getId()), build.resolvePlatform(), build.getTriggered().getUser().getUserName(), run.getEnvironment());
            builds.add(build);
        }
        return builds;
    }

    public static void verifyMainBuildStatus(List<String> statuses) {
        for (String status : statuses) {
            if (!status.equals("SUCCESS")) {
                System.exit(1);
            }
        }
    }
}
